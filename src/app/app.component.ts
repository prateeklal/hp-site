import { Component, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { ProfileService } from './profile.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private ngFire: AngularFireAuth, private router: Router, private zone: NgZone, private profile: ProfileService) {
    this.ngFire.auth.onAuthStateChanged((user) => {
      if (user) {
        this.profile.userDetails = {...user.providerData[0]};
        this.zone.run(() => {
          this.router.navigate(['/home']);
        });
      } else {
        this.profile.userDetails = {};
        console.log('Auth State Changed! User Not Available!')
      }
    });
  }

  ngOnInit() {
  }
}

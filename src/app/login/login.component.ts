import { Component, OnInit, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private ngFire: AngularFireAuth, private router: Router, private zone: NgZone) {
  }
  
  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(8)]),
  });

  authFailError = '';
  
  get email() { return this.loginForm.get('email'); }
  get password() { return this.loginForm.get('password'); }

  loginUser() {
    console.log(this.email.value, this.password.value);
    const email = this.email.value;
    const password = this.password.value;
    // const authFailError = this.authFailError;

    this.ngFire.auth.signInWithEmailAndPassword(email, password)
    .then((response) => {
      console.log(response);
      this.router.navigateByUrl('/home');
    })
    .catch((error) => {
      let errorCode = error.code;
      let errorMessage = error.message;
      switch (errorCode) {
        case 'auth/wrong-password' : {
          this.authFailError = 'Incorrect password';
          break;
        };
        case 'auth/invalid-email': {
          this.authFailError = 'Please enter a correct email address';
          break;
        };
        case 'auth/user-not-found': {
          this.authFailError = 'User not found';
          break;
        };
        default: {
          console.log(errorMessage);
        }
      }
      console.log(error);
    });
  };

  loginWithGoogle() {
    this.ngFire.auth.signInWithRedirect(new firebase.auth.GoogleAuthProvider())
    .catch(error => {
      console.log(error);
      this.authFailError = error.message;
    });
  };
  
  ngOnInit() {
  };

}

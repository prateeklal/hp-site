import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  get userDetails(): object {
    const userData = sessionStorage.getItem('userDetails');
    return JSON.parse(userData);
  }

  set userDetails(userObj: object) {
    if(Object.keys(userObj).length) {
      sessionStorage.setItem('userDetails', JSON.stringify(userObj))
    } else {
      sessionStorage.removeItem('userDetails');
    }
  }
}

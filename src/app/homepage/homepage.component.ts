import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { ProfileService } from '../profile.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  constructor(private ngFire: AngularFireAuth, private router: Router, private profile: ProfileService) { }

  userDetails = this.profile.userDetails;

  signOutUser() {
    this.ngFire.auth.signOut()
    .then(() => {
      this.router.navigate(['']);
    });
  }

  ngOnInit() {
  }

}

## Setup

Run `npm i` to install the application's dependencies.

## Development server

Please make sure you have angular cli installed in your machine. `npm install -g @angular/cli`.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Templating

We are using [Angular Material](https://material.angular.io) and [Angular Flex](https://github.com/angular/flex-layout) in this project.

For iconography please refer to [Material Icons](https://material.io/tools/icons/?style=baseline).

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
